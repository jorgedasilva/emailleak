﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test2.Models
{
    public class Email
    {
        public int Id { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Sender { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Receiver { get; set; }

        [StringLength(300, MinimumLength = 3)]
        public string Subject { get; set; }

        [StringLength(5000, MinimumLength = 5)]
        [Required]
        public string Body { get; set; }

        [Display(Name = "E-mail Sent on...")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode =true)]
        public DateTime SentDate { get; set; } = DateTime.Now;


    }
}
