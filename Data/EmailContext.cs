﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Test2.Models;

namespace Test2.Data
{
    public class EmailContext : DbContext
    {
        public EmailContext (DbContextOptions<EmailContext> options)
            :base(options)
        {

        }
        public DbSet<Email> Email { get; set; }
    }
}

//https://docs.microsoft.com/pt-pt/aspnet/core/tutorials/first-mvc-app/adding-model?view=aspnetcore-3.0&tabs=visual-studio