﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test2.Data;
using Test2.Models;
using Microsoft.AspNetCore.Http;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Test2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly EmailContext _context;
        private string Message;


        public object Session { get; private set; }

        public HomeController(ILogger<HomeController> logger, EmailContext context)
        {
            _logger = logger;
            _context = context;

        }

        public IActionResult Index()
        {
            var DateSent = HttpContext.Session.GetString("message");
            
            ViewData["products"] = DateSent;
            HttpContext.Session.Clear();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(Models.Email formData)
        {
            if (!ModelState.IsValid)
            {
                return View(formData);
            }
            else
            {
               
                _context.Add(formData);
                await _context.SaveChangesAsync();

                string dataMessage = _context.Email.Find(formData.Id).SentDate.ToString();
                HttpContext.Session.SetString("message", "Your E-mail was sent on " + dataMessage + ". Congrats. Literally no one can do that.");

                var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY");           
                var client = new SendGridClient(apiKey);                
                var from = new EmailAddress(formData.Sender);               
                var subject = formData.Subject;              
                var to = new EmailAddress(formData.Receiver);
                var htmlContent = formData.Body;
                var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);
                var response = await client.SendEmailAsync(msg);

                return RedirectToAction(nameof(Index));
                
            }

            //ModelState.Clear();
            //return View(email);
        }

   

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
